package com.jagan.DemoHB;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class App_HQL {

	public static void main(String[] args) {
		Configuration con=new Configuration().configure().addAnnotatedClass(Student1.class);
		ServiceRegistry sr= new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
		SessionFactory sf=con.buildSessionFactory(sr);
		Session ses1 =sf.openSession();
		ses1.beginTransaction();
		/*Random r =new Random();
		for(int i=1;i<=50;i++)
		{
			Student1 std = new Student1();
			std.setRollNo(i);
			std.setName("name"+i);
			std.setMarks(r.nextInt(100));
			ses1.save(std);
		}
		*/
		
		/*Query q = ses1.createQuery("from Student1 where rollNo=1 AND marks>=50");
		Student1 list =(Student1)q.uniqueResult();
		
			System.out.println(list);
		*/
		
//		Native Queries
		
		SQLQuery q = ses1.createSQLQuery("select name,marks from student1 where marks>30");
//		q.addEntity(Student1.class);
		q.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		 Criteria c=ses1.createCriteria(Student1.class);
		List<Object> s = q.list();
		for(Object st:s)
		{
			Map m= (Map) st;
			System.out.println(m.get("name") +" :"+ m.get("marks"));
		}
		ses1.getTransaction().commit();
		ses1.close();
	}

}
