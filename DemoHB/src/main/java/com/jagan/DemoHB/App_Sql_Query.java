package com.jagan.DemoHB;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class App_Sql_Query {

	public static void main(String[] args) {

		
		Alien a = null;
		Configuration con=new Configuration().configure().addAnnotatedClass(Alien.class);
		ServiceRegistry sr= new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
		SessionFactory sf=con.buildSessionFactory(sr);
		Session ses1 =sf.openSession();
		ses1.beginTransaction();
		Query qur = ses1.createQuery("from alien_table where aid=1");
		qur.setCacheable(true);
		a=(Alien)qur.uniqueResult();
		System.out.println(a);
		ses1.getTransaction().commit();
		ses1.close();
	
		Session ses2 =sf.openSession();
		ses2.beginTransaction();
		Query qur1 = ses2.createQuery("from alien_table where aid=1");
		qur1.setCacheable(true);
		a=(Alien)qur1.uniqueResult();
		System.err.println(a);
		ses2.getTransaction().commit();
		ses2.close();

	}

}
