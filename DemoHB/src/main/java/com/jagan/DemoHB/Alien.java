package com.jagan.DemoHB;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity (name ="alien_table")
//@Table(name ="alien_table")
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)

public class Alien {
	 
	@Id
	private int aid;	
	private Alien_Name aName;
	private String aColor;

	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public Alien_Name getaName() {
		return aName;
	}
	public void setaName(Alien_Name aName) {
		this.aName = aName;
	}
	public String getaColor() {
		return aColor;
	}
	public void setaColor(String aColor) {
		this.aColor = aColor;
	}
	@Override
	public String toString() {
		return "Alien [aid=" + aid + ", aName=" + aName + ", aColor=" + aColor + "]";
	}
	
	

}
