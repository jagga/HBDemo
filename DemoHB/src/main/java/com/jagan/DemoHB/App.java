package com.jagan.DemoHB;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Alien_Name an= new Alien_Name();
    	an.setfName("jagannth");
    	an.setlName("Choudhury");
    	an.setmName("kumar");
        Alien al= new Alien();
        al.setAid(03);
        al.setaName(an);
        al.setaColor("Blue"); 
        
        Configuration con=new Configuration().configure().addAnnotatedClass(Alien.class);
        
        ServiceRegistry sr= new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
        
        SessionFactory sf=con.buildSessionFactory(sr);
        Session ses =sf.openSession();
        
        Transaction txn= ses.beginTransaction();
        ses.save(al);
        txn.commit();
    }
}
