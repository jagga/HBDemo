package com.jagan.DemoHB;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class App_Cache {
	public static void main(String[] args) {
		
		Alien a = null;
		Configuration con=new Configuration().configure().addAnnotatedClass(Alien.class);
		ServiceRegistry sr= new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
		SessionFactory sf=con.buildSessionFactory(sr);
		Session ses1 =sf.openSession();
		ses1.beginTransaction();
		a= (Alien) ses1.get(Alien.class, 1);
		System.out.println(a.getaName().getlName());
		System.out.println(a.getaName().getfName());
		ses1.getTransaction().commit();
		ses1.close();
		
		
		Session ses2 =sf.openSession();
		ses2.beginTransaction();		
		System.err.println(((Alien)ses2.get(Alien.class, 1)).getaName().getfName());
		ses2.getTransaction().commit();
		ses2.close();
	}

}
