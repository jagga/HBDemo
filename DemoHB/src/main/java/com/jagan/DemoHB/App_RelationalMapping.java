package com.jagan.DemoHB;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class App_RelationalMapping {

	public static void main(String[] args) {
		
		laptop lp= new laptop();
		lp.setLid(101);
		lp.setLname("Dell");
		
		Student std= new Student();
		std.setRollNo(1);
		std.setsName("jagan");
		std.setMarks(20);
		std.getLap().add(lp);
		lp.getStudent().add(std);
        
        Configuration con=new Configuration().configure().addAnnotatedClass(Student.class).addAnnotatedClass(laptop.class);
        
        
        ServiceRegistry sr= new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
        
        SessionFactory sf=con.buildSessionFactory(sr);
        Session ses =sf.openSession();
        
        Transaction txn= ses.beginTransaction();
        ses.save(lp);
        ses.save(std);
        txn.commit();

	}

}
