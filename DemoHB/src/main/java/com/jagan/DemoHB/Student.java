package com.jagan.DemoHB;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Student {
	
	@Id
	private int rollNo;
	private String sName;
	private int marks;
	@ManyToMany(mappedBy ="student")
	private List<laptop> lap = new ArrayList<laptop>();
	
	
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public int getMarks() {
		return marks;
	}
		
	public List<laptop> getLap() {
		return lap;
	}
	public void setLap(List<laptop> lap) {
		this.lap = lap;
	}
	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", sName=" + sName + ", marks=" + marks + "]";
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	

}
